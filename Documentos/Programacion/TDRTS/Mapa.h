#pragma once
#include <iostream>
#include <deque>
#include "Constantes.h"
#include <stdlib.h>
#include <time.h>
#include "Estructuras.h"


class Mapa
{
	public:
		Mapa();
		~Mapa();
		int getMapa(int x, int y);

		std::deque<Estructuras*> estructuras;
	private:
		int numero;
		int mapa[MAPA_H][MAPA_W];
		void resetMapa();	
		
		
};
