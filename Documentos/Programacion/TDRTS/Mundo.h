#pragma once
#include <iostream>
#include <deque>
#include "Constantes.h"
#include <stdlib.h>
#include <time.h>
#include "Mapa.h"


class Mundo
{
	public:
		Mundo();
		~Mundo();
		int getMundo(int x, int y);
		bool randomizar();
		void resetMundo();
		std::deque<Mapa*> mapas;
	private:
		int numero;
		int mundo[MUNDO_H][MUNDO_W];
		//void resetMundo();

		//bool randomizar();

		int Anodo,Tnodo,Pnodo,Mnodo;
		
		
};
