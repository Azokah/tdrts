#pragma once
#include <iostream>
#include <deque>
#include <string.h>
#include "Personaje.h"
#include "Mapa.h"
#include <cstdlib>

enum enComandos
{
	LOGOUT,
	ESPERAR,
	AGARRAR,
	MIRAR,
	SOLTAR,
	BINDEAR,
	NORTE,
	SUR,
	ESTE,
	OESTE,
	DECIR
};

struct stComandos
{
	std::string comando;
	enComandos codigo;
	std::deque<std::string> bindeo;
};


class Comandos {
	public:
		Comandos(Personaje * PJ, Mapa * MAPA, bool * GAMEON);
		~Comandos();
		
		void cmd(std::string comando);//Se va a cambiar de consola a SDL
				

	private:
	
		void agarrar(std::string comando, int strInt);
		void mirar();
		void logout();
		void bindear(std::string comando, std::string bind);		
		void decir(std::string oracion);
		std::string getCompuesto(std::string * comando );
		void getOracion(std::string * oracion); // No Utilizar, usar getCompuesto instead.
		
		//Movimiento
		void norte();
		void sur();
		void este();
		void oeste();

		std::deque<stComandos> listaComandos;

		bool * gameOn;
		Personaje * pj;
		Mapa * mapa;

};

