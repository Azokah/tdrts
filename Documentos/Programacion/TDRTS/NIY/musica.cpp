#include "musica.h"


musica::musica(void)
{
	_musica = NULL;
	_chunk = NULL;
	_misil = NULL;
	_volumen = 100;
}


musica::~musica(void)
{
}
void musica::init()
{
	if(Mix_OpenAudio(AUDIO_HZ, MIX_DEFAULT_FORMAT,AUDIO_CHANNELS,AUDIO_CHUNK_SIZE) == -1)
	{
		//getError("No se pudo inicializar el Mixer.");
		std::cout << "No se cargo el audio" << std::endl;
	}

	_musica = Mix_LoadMUS("Assets/Audios/Musica/City1.mp3");
	if(_musica == NULL)
	{
		//getError("No se pudo cargar la musica.");
	}
	Mix_VolumeMusic(_volumen);
}
int musica::getVolume()
{
	return _volumen;
}
void musica::tocar()
{
	if(Mix_PlayingMusic() == 0)
	{
		Mix_PlayMusic(_musica, -1);
	}
	
}

void musica::setVolume(int x)
{

	if(_volumen+x <= 100 && _volumen+x >= 0)
	{
		_volumen = _volumen+x;
		Mix_VolumeMusic(_volumen);
	}
}