#include "Comandos.h"

Comandos::Comandos(Personaje * PJ, Mapa * MAPA, bool * GAMEON)
{
	pj = PJ;
	mapa = MAPA;
	gameOn = GAMEON;

	stComandos cmdo;
	cmdo.comando = "logout";
	cmdo.codigo = LOGOUT;
	listaComandos.push_back(cmdo);
	cmdo.comando = "mirar";
	cmdo.codigo = MIRAR;
	listaComandos.push_back(cmdo);
	cmdo.comando ="agarrar";
	cmdo.codigo = AGARRAR;
	listaComandos.push_back(cmdo);
	cmdo.comando = "bind";
	cmdo.codigo = BINDEAR;
	listaComandos.push_back(cmdo);
	cmdo.comando = "decir";
	cmdo.codigo = DECIR;
	listaComandos.push_back(cmdo);
	cmdo.comando = "norte";
	cmdo.codigo = NORTE;
	listaComandos.push_back(cmdo);
	cmdo.comando = "sur";
	cmdo.codigo = SUR;
	listaComandos.push_back(cmdo);
	cmdo.comando = "oeste";
	cmdo.codigo = OESTE;
	listaComandos.push_back(cmdo);
	cmdo.comando = "este";
	cmdo.codigo = ESTE;
	listaComandos.push_back(cmdo);

};

//Esta input en modo consola, pasar a modo SDL
void Comandos::cmd(std::string comando)
{
	/* MODO CONSOLA INPUT */
	std::string compuesto;
	/* END MODO CONSOLA INPUT */

	/* Determinar si es una palabra compuesta*/
	compuesto = getCompuesto(&comando);
	// Si la palabra es un numero //
	int strInt;
	strInt = atoi(compuesto.c_str());
	/* termina determinacion de palabra compuesta*/
	for(int i = 0; i < listaComandos.size(); i++)
	{
		bool match = false;
		for( int k = 0; k < listaComandos.at(i).bindeo.size(); k++)
		{
			if( comando == listaComandos.at(i).bindeo.at(k) ) match = true;
		}
		if(comando == listaComandos.at(i).comando || match )
		{
			switch(listaComandos.at(i).codigo)
			{
				case LOGOUT:
					logout();
					break;
				case ESPERAR:
					std::cout<<"No haces nada."<<std::endl;
					break;
				case MIRAR:
					mirar();
					break;
				case AGARRAR:
					agarrar(compuesto, strInt);
					break;
				case BINDEAR:
					comando = getCompuesto(&compuesto);
					bindear(compuesto,comando);
					break;
				case DECIR:
					//getOracion(&comando); ya lo tenia hecho -.-
					decir(compuesto);
					break;
				case NORTE:
					norte();
					break;
				case SUR:
					sur();
					break;
				case OESTE:
					oeste();
					break;
				case ESTE:
					este();
					break;
				default:
					break;
			}
		}
	}
};

void Comandos::decir(std::string oracion)
{
	std::cout<<"Dices: "<<oracion<<std::endl;
}


void Comandos::agarrar(std::string comando, int strInt)
{
	if(strInt != 0 && strInt >= 0 && strInt < mapa->getObjetos()->size()+1)
	{
		strInt = strInt - 1;
		pj->agarrar(mapa->getObjetos()->at(strInt).objeto,mapa->getObjetos()->at(strInt).x,mapa->getObjetos()->at(strInt).y);
		mapa->getObjetos()->erase(mapa->getObjetos()->begin()+strInt);
	}
	else
	{
		for(int w = 0; w < mapa->getObjetos()->size(); w++)
		{
			if(mapa->getObjetos()->at(w).objeto->getNombre() == comando)
			{
			
				pj->agarrar(mapa->getObjetos()->at(w).objeto,mapa->getObjetos()->at(w).x,mapa->getObjetos()->at(w).y);
				mapa->getObjetos()->erase(mapa->getObjetos()->begin()+w);
				w = mapa->getObjetos()->size();
			}
		}
	}
};

void Comandos::mirar(){
	//pj->mirar(NULL);
	for(int w = 0; w < mapa->getObjetos()->size(); w++)
	{
		std::cout<<w+1<<": "<<mapa->getObjetos()->at(w).objeto->getNombre()<<" - "<< mapa->getObjetos()->at(w).objeto->getDesc()<<std::endl;
	}
};


void Comandos::logout()
{
	*gameOn = false;
};

void Comandos::bindear(std::string comando, std::string bind)
{
	bool usado = false;
	for(int w = 0; w < listaComandos.size(); w++)
	{
		for(int k = 0; k < listaComandos.at(w).bindeo.size(); k++)
		{
			if( listaComandos.at(w).bindeo.at(k) == bind)
			{
				std::cout<<"Bindeo ya usado."<<std::endl;
				usado = true;
			}
		};
		
	};
	if( usado == false) {
	for(int w = 0; w < listaComandos.size(); w++)
	{
		if(listaComandos.at(w).comando == comando)
		{
			listaComandos.at(w).bindeo.push_back(bind);
			std::cout<<"Se ha bindeado: "<<listaComandos.at(w).comando<<" por: "<<bind<<"."<<std::endl;
		};
	};
	};
};

std::string Comandos::getCompuesto(std::string * comando )
{
	std::string compuesto;
	std::size_t espacio =  comando->find(" ");
	if(espacio != std::string::npos)
	{
		compuesto = comando->substr(espacio+1);
		*comando = comando->substr(0,espacio);
	}

	return compuesto;
};

void Comandos::getOracion(std::string * oracion)
{
	//NO UTILIZAR
	std::string retornar;
	std::size_t espacio = oracion->find(" ");
	std::cout<<"El retorno del rey retorna: "<< *oracion<<std::endl;
	if(espacio != std::string::npos)
	{
		retornar = oracion->erase(0,oracion->find(" ")+1);
	}
}

void Comandos::norte()
{
	pj->moveTo(pj->getX(),pj->getY()-1);	
	std::cout<<"Te mueves al norte."<<std::endl;
};
void Comandos::sur()
{
	pj->moveTo(pj->getX(),pj->getY()+1);
	std::cout<<"Te mueves al sur."<<std::endl;
};
void Comandos::este()
{
	pj->moveTo(pj->getX()+1,pj->getY());
	std::cout<<"Te mueves al este."<<std::endl;
};
void Comandos::oeste()
{
	pj->moveTo(pj->getX()-1,pj->getY());
	std::cout<<"Te mueves al oeste."<<std::endl;
};
