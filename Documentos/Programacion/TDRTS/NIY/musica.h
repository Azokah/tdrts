#pragma once

#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL.h>
#include "getError.h"
#include <iostream>
#include "Constantes.h"

class musica
{
public:
	musica(void);
	~musica(void);
	void init();
	void tocar();
	void setVolume(int x);
	void Chunk();
	void Misil();
	int getVolume();
private:
	Mix_Music * _musica;
	Mix_Chunk * _chunk, * _misil;
	int _volumen;
};

