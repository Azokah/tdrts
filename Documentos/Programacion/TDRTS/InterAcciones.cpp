#include "InterAcciones.h"

InterAcciones::InterAcciones(){
	
	quitOn = camaraNorte = vistaInt = camaraSur = camaraOeste = camaraEste = false;
	resetMondo = false;
};
InterAcciones::~InterAcciones(){};

void InterAcciones::run(bool * gameOn,bool * vistaInterna,Camara * camara){
	if(camaraNorte) camara->setCamaraXY(camara->getX(),camara->getY()-VELOCIDAD_CAMARA);
	if(camaraSur) camara->setCamaraXY(camara->getX(),camara->getY()+VELOCIDAD_CAMARA);
	if(camaraOeste) camara->setCamaraXY(camara->getX()-VELOCIDAD_CAMARA,camara->getY());
	if(camaraEste) camara->setCamaraXY(camara->getX()+VELOCIDAD_CAMARA,camara->getY());
	if(quitOn) *gameOn = false;
	if(vistaInt) *vistaInterna = true;
	else *vistaInterna = false;
};
void InterAcciones::moverCamaraNorte(bool x){camaraNorte = x;};
void InterAcciones::moverCamaraSur(bool x){camaraSur = x;};
void InterAcciones::moverCamaraEste(bool x){camaraEste = x;};
void InterAcciones::moverCamaraOeste(bool x){camaraOeste = x;};
void InterAcciones::quit(){quitOn = true;};
void InterAcciones::cambiarVistaInt(bool x){ vistaInt = x; };

void InterAcciones::resetMundo(){ resetMondo = true; };

bool InterAcciones::getResetMondo(){if(resetMondo){ resetMondo = false; return true;} else { return false; }};

