#include "Mundo.h"


Mundo::Mundo()
{
	srand(time(NULL));
	numero = rand()%MAPA_ARBOLES_C;
	
	Anodo = NODOS_AGUA;
	Tnodo = NODOS_TIERRA;
	Pnodo = NODOS_PASTO;
	Mnodo = NODOS_MONTE;
	
	resetMundo();
	//while(!randomizar()){};
	
	mapas.push_back(new Mapa());
	mapas.front()->estructuras.push_back(new Torre(3,2));
	mapas.front()->estructuras.push_back(new Granja(4,2));
	mapas.front()->estructuras.push_back(new Torre(5,2));
	for(int i = 0; i< numero; i++)
	{
		mapas.front()->estructuras.push_back(new Arbusto((rand()%(MAPA_W-2))+1,(rand()%(MAPA_H-2))+1));
	}
};

Mundo::~Mundo(){};
bool Mundo::randomizar()
{
	bool arriba, abajo, izquierda, derecha;
	arriba = abajo = izquierda = derecha = false;
	for (int i = 0; i < MUNDO_H; i++)
	{
		for (int j = 0; j <MUNDO_W; j++)
		{
			if (mundo[i][j] != MUNDO_VACIO)
			{
				int prob;
				switch (mundo[i][j])
				{
				case MUNDO_TIERRA:
					prob = PROB_TIERRA;
					break;
				case MUNDO_PASTO:
					prob = PROB_PASTO;
					break;
				case MUNDO_AGUA:
					prob = PROB_AGUA;
					break;
				case MUNDO_MONTE:
					prob = PROB_MONTE;
					break;
				}
				//if (!arriba)
				{
					if (mundo[i - 1][j] == MUNDO_VACIO && i - 1 >= 0 && i - 1 < MUNDO_H)
					{
						if (rand() % 100 <= prob)
						{
							mundo[i - 1][j] = mundo[i][j];
							arriba = true;
						}
					}
				}
				//if (!abajo)
				{
					if (mundo[i + 1][j] == MUNDO_VACIO && i + 1 >= 0 && i + 1 < MUNDO_H)
					{
						if (rand() % 100 <= prob)
						{
							mundo[i + 1][j] = mundo[i][j];
							abajo = true;
						}
					}
				}
				//if (!izquierda)
				{
					if (mundo[i][j - 1] == MUNDO_VACIO && j - 1 >= 0 && j - 1 < MUNDO_W)
					{
						if (rand() % 100 <= prob)
						{
							mundo[i][j - 1] = mundo[i][j];
							izquierda = true;
						}
					}
				}
				//if (!derecha)
				{
					if (mundo[i][j + 1] == MUNDO_VACIO && j + 1 >= 0 && j + 1 < MUNDO_W)
					{
						if (rand() % 100 <= prob)
						{
							mundo[i][j + 1] = mundo[i][j];
							derecha = true;
						}
					}
				}
			}
		}
	}
	for (int i = 0; i < MUNDO_H; i++)
	{
		for (int j = 0; j < MUNDO_W; j++)
		{
			if (mundo[i][j] == MUNDO_VACIO) return false;
		}
	}
	return true;
}

/*
void Mundo::diaNoche()
{
	r--;
	g--;
	if (r < 0 || g < 0)
	{
		r = g = b = 255 * DIANOCHEMOD;
	}
	else SDL_SetTextureColorMod(t_tileset, r / DIANOCHEMOD, g / DIANOCHEMOD, b / DIANOCHEMOD);
}
*/
void Mundo::resetMundo()
{
	for(int i = 0;i < MUNDO_H; i++)
	{
		for(int j = 0;j < MUNDO_W; j++)
		{
			mundo[i][j] = MUNDO_VACIO;
			//if( i == 0 || i == MUNDO_H -1 || j == 0 || j == MUNDO_W -1) mundo[i][j] = MUNDO_AGUA;

		}

	}

	for (int i = 0; i < Tnodo; i++)
	{
		mundo[rand() % MUNDO_H][rand() % MUNDO_W] = MUNDO_TIERRA;
	}

	for (int i = 0; i < Pnodo; i++)
	{
		mundo[rand() % MUNDO_H][rand() % MUNDO_W] = MUNDO_PASTO;
	}

	for (int i = 0; i < Anodo; i++)
	{
		mundo[rand() % MUNDO_H][rand() % MUNDO_W] = MUNDO_AGUA;
	}

	for (int i = 0; i < Mnodo; i++)
	{
		mundo[rand() % MUNDO_H][rand() % MUNDO_W] = MUNDO_MONTE;
	}
};

int Mundo::getMundo(int x, int y)
{
	return mundo[y][x];
};


