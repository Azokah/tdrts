#pragma once
#include <iostream>
#include <SDL2/SDL.h>
#include "Constantes.h"
#include <string>

enum estructurasTipo {
	GRANJA,
	BARRACAS,
	MURALLA,
	TORRE,
	ARBUSTO
};

class Estructuras {
	public:

		//Estructuras();
		virtual ~Estructuras();
		
		void init();

		void construir();
		void demoler();			
		
		estructurasTipo getTipo();	

		int getX();
		int getY();
	protected:
		int x, y;
		estructurasTipo tipo;	
};


class Granja : public Estructuras {
	public:
		Granja(int X, int Y);
		~Granja();
};

class Barracas : public Estructuras {
	public:
		Barracas(int X, int Y);
		~Barracas();
};

class Muralla : public Estructuras {
	public:
		Muralla(int X, int Y);
		~Muralla();
};

class Torre : public Estructuras {
	public:
		Torre(int X, int Y);
		~Torre();
};

class Arbusto : public Estructuras {
	public:
		Arbusto(int X, int y);
		~Arbusto();
};
