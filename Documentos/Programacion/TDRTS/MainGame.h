#pragma once
#include <iostream>
#include <string.h>
#include <deque>
#include "Constantes.h"
#include "Mapa.h"
#include "SdlHelper.h"
#include "Timer.h"
#include "Camara.h"
#include "InterAcciones.h"
#include "Estructuras.h"
#include "Mundo.h"

class MainGame {
	public:
		MainGame();
		~MainGame();
		void init();

	private:

		bool vistaInterna;

		void pasarTurno();
		int proximoTick;
		bool gameOn;
		void drawFPS();
		
		int FPS,FPSMAX;
		//Clases
		Mundo * mundo;
		SdlHelper * sdl;
		Timer * timer;
		Camara * camara;			
		InterAcciones * interfaz;
};	
