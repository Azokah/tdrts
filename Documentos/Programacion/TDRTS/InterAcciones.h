#pragma once
#include <iostream>
#include "Camara.h"
#include "Constantes.h"

class InterAcciones {
	public:
		InterAcciones();
		~InterAcciones();

		void run(bool * gameOn,bool * vistaInterna,Camara * camara);
		void moverCamaraNorte(bool);
		void moverCamaraSur(bool);
		void moverCamaraEste(bool);
		void moverCamaraOeste(bool);
		

		void resetMundo();

		bool getResetMondo();
		void cambiarVistaInt(bool);
		void quit();
	private:
		bool quitOn, vistaInt, camaraNorte, camaraSur, camaraEste, camaraOeste;
		bool resetMondo;
};

