#pragma once
#include <iostream>
#include "Constantes.h"

class Camara {

	public:
		Camara();
		~Camara();

		int getX();
		int getY();
		
		int getW();
		int getH();

		void setCamaraXY(int,int);
	private:
	
		int x, y, w, h;
};
