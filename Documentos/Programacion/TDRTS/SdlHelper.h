#pragma once
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "Constantes.h"
#include <string.h>
#include "Mapa.h"
#include "getError.h"
#include "Texto.h"
#include "Camara.h"
#include "Hud.h"
#include "InterAcciones.h"
#include "Mundo.h"
class SdlHelper {

	public:
		SdlHelper();
		~SdlHelper();

			
		void updateMundo(bool vistaInterna, Mundo * mundo,Mapa * mapa, int y, int x, Camara * camara,InterAcciones * interfaz);
		void limpiarRender();
		void renderizar();

		void input(InterAcciones * interfaz);
		void imprimirTexto(std::string mensaje, int x, int y, int r, int g, int b);
	
			
		
	
	private:
		
		void updateObjetos(Mapa * mapa, int y, int x, Camara * camara);

		SDL_Texture * cargarTextura(SDL_Renderer * renderer, std::string path);

		SDL_Window * ventana;
		SDL_Renderer * render;

		SDL_Texture * tileset;
		SDL_Rect  rectDestino,  rectZona;
		
		bool debugOn;

		void moverCamara(InterAcciones * interfaz);
			
		Hud * hud;
		Texto * texto;
};
