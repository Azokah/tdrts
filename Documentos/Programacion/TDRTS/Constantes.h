//Constantes de juego


//Constantes SDL
#define PANTALLA_AL 768
#define PANTALLA_AN 1366
#define TITULO_VENTANA "Tiempos de conquista - TOC - V1.0.0"
#define ASSETS_PATH "Assets/Tileset.png"
#define TILE_AL 64
#define TILE_AN 64
#define HUD_PATH "Assets/HUD.png"

//Constantes Camara
#define CAMARA_AN PANTALLA_AN
#define CAMARA_AL PANTALLA_AL
#define VELOCIDAD_CAMARA 24
#define BORDE_CAMARA 25


//Constantes Tileset
#define TILE_PAREDY 0
#define TILE_PAREDX 1
#define TILE_PASTOX 0
#define TILE_PASTOY 0
#define TILE_PJX 3
#define TILE_PJY 1
#define TILE_AGUAX 3
#define TILE_AGUAY 0
#define TILE_GRANJAX 1
#define TILE_GRANJAY 1
#define TILE_TORREX 2
#define TILE_TORREY 1
#define TILE_ARBUSTOX 0
#define TILE_ARBUSTOY 1
#define TILE_TIERRAX 2
#define TILE_TIERRAY 0


// Constantes de Mundo
#define NODOS_AGUA 3 
#define PROB_AGUA 50
#define NODOS_PASTO 4
#define PROB_PASTO 70
#define NODOS_TIERRA 2
#define PROB_TIERRA 90
#define NODOS_MONTE 3
#define PROB_MONTE 50
#define MUNDO_W 100
#define MUNDO_H 50
#define MUNDO_PASTO 1
#define MUNDO_AGUA 0
#define MUNDO_TIERRA 2
#define MUNDO_MONTE 3
#define MUNDO_VACIO -1

// Constantes del mapa
#define MAPA_W 50
#define MAPA_H 50
#define MAPA_PASTO 0
#define MAPA_PARED 1
#define MAPA_ARBOLES_C 200


//Constantes del personaje
#define PJ_NAME 25

//Constantes de la Fuente
#define TEXTO_SIZE 12
#define TEXTO_ALPHA 0
#define TEXTO_FUENTE "Assets/ARCADE_N.TTF"
