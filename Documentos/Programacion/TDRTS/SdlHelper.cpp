#include "SdlHelper.h"

SdlHelper::SdlHelper()
{
	debugOn = false;
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0 ) getError("No se pudo inicializar SDL.");
	ventana = SDL_CreateWindow(TITULO_VENTANA, SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,PANTALLA_AN,PANTALLA_AL,SDL_WINDOW_SHOWN);
	render = SDL_CreateRenderer(ventana,-1,SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);

	tileset = cargarTextura(render,ASSETS_PATH);

	rectDestino.x = rectDestino.y = rectZona.x = rectZona.y = 0;
	rectDestino.h = TILE_AL;
	rectDestino.w = TILE_AN;
	rectZona.h = rectZona.w = TILE_AN;

	texto = new Texto();
	texto->init(TEXTO_SIZE);
	texto->setAlpha(TEXTO_ALPHA);
	hud = new Hud();
	hud->setTexture(cargarTextura(render,HUD_PATH));
};
SdlHelper::~SdlHelper(){};

void SdlHelper::imprimirTexto(std::string mensaje, int x, int y, int r, int g, int b)
{

	texto->ponerTexto(mensaje,x,y,r,g,b,render);

}

SDL_Texture * SdlHelper::cargarTextura(SDL_Renderer * renderer, std::string path)
{
	SDL_Texture * textura;
	textura = IMG_LoadTexture(renderer,path.c_str());
	if( textura == NULL)
	{
		getError("No se pudo cargar la textura:" + path);
		return nullptr;
	}
	return textura;

};
void SdlHelper::limpiarRender(){ SDL_RenderClear(render);};
void SdlHelper::renderizar(){
	//NoQuieroDibujar el HUD SDL_RenderCopy(render,hud->getTexture(),NULL,NULL);
	SDL_RenderPresent(render);
};
void SdlHelper::updateMundo(bool vistaInterna, Mundo * mundo, Mapa * mapa, int y, int x, Camara * camara,InterAcciones * interfaz)
{
	//Dibujo dentro de los confines de la camara
	if(x*TILE_AN >= camara->getX()-TILE_AN && x*TILE_AN <= camara->getX()+camara->getW()){
		if(y*TILE_AL >= camara->getY()-TILE_AL && y*TILE_AL <= camara->getY()+camara->getH()){
			if(vistaInterna && x < MAPA_W && y < MAPA_H){
				if(mapa->getMapa(x,y) == MAPA_PARED){
					rectZona.x = TILE_PAREDX*TILE_AN;
					rectZona.y = TILE_PAREDY*TILE_AL;
				}else {
					rectZona.x = TILE_PASTOX*TILE_AN;
					rectZona.y = TILE_PASTOY*TILE_AL;
				}
			} else {
				if(mundo->getMundo(x,y) == MUNDO_AGUA) {
					rectZona.x = TILE_AGUAX*TILE_AN;
					rectZona.y = TILE_AGUAY*TILE_AL;
				} else if(mundo->getMundo(x,y) == MUNDO_PASTO){
					rectZona.x = TILE_PASTOX*TILE_AN;
					rectZona.y = TILE_PASTOY*TILE_AL;
				} else if(mundo->getMundo(x,y) == MUNDO_MONTE){
					rectZona.x = TILE_PAREDX*TILE_AN;
					rectZona.y = TILE_PAREDY*TILE_AL;
				} else if(mundo->getMundo(x,y) == MUNDO_TIERRA){
					rectZona.x = TILE_TIERRAX*TILE_AN;
					rectZona.y = TILE_TIERRAY*TILE_AL;
				}
				else {
					rectZona.x = TILE_GRANJAX*TILE_AN;
					rectZona.y = TILE_GRANJAY*TILE_AL;
				}

			}

			rectDestino.x =  (x * TILE_AN ) - camara->getX();
			rectDestino.y =  (y * TILE_AL ) - camara->getY();

			SDL_RenderCopy(render,tileset,&rectZona,&rectDestino);
		
			if(vistaInterna) updateObjetos(mapa,y,x,camara);
			
			//Aca abajo deberia hacerse en funcion aparte Dibujar elementos HUD
			//dibujo va aca
			//Tambien estoy dibujando textos desde el MAIN y deben ser parte del HUD
			char txt[10];
			sprintf(txt,"%d-%d",x,y);
			if(debugOn)imprimirTexto(txt,rectDestino.x,rectDestino.y,255,125,0);
			
		}	
	}

}

void SdlHelper::input(InterAcciones * interfaz)
{
	moverCamara(interfaz);
	
	SDL_Event  event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_KEYDOWN:

				switch( event.key.keysym.sym ){

					case SDLK_ESCAPE:
						interfaz->quit();	
						break;
					case SDLK_UP:
						interfaz->moverCamaraNorte(true);
						break;
					case SDLK_DOWN:
						interfaz->moverCamaraSur(true);
						break;
					case SDLK_LEFT:
						interfaz->moverCamaraOeste(true);
						break;
					case SDLK_RIGHT:
						interfaz->moverCamaraEste(true);
						break;
					case SDLK_F1:
						if(debugOn) debugOn = false;
						else debugOn = true;
						break;
					case SDLK_2:
						interfaz->cambiarVistaInt(true);
						break;
					case SDLK_1:
						interfaz->cambiarVistaInt(false);
						break;
					case SDLK_SPACE:
						interfaz->resetMundo();
						break;
					default:
						break;
				}
				break;
			case SDL_QUIT:
				SDL_Quit();
				interfaz->quit();
				break;
			case SDL_MOUSEBUTTONDOWN:
				switch(event.button.button)
				{
					case SDL_BUTTON_LEFT:
						break;
					case SDL_BUTTON_RIGHT:
						break;
					default:
						break;
				}
				break;
		}
	}
}

void SdlHelper::moverCamara(InterAcciones * interfaz){
	int  mousex, mousey;
	SDL_GetMouseState(&mousex, &mousey);
	interfaz->moverCamaraOeste(false);
	interfaz->moverCamaraEste(false);
	interfaz->moverCamaraNorte(false);
	interfaz->moverCamaraSur(false);
	if(mousex < BORDE_CAMARA) interfaz->moverCamaraOeste(true);
	if(mousex > CAMARA_AN-BORDE_CAMARA) interfaz->moverCamaraEste(true);
	if(mousey < BORDE_CAMARA) interfaz->moverCamaraNorte(true);
	if(mousey > CAMARA_AL-BORDE_CAMARA) interfaz->moverCamaraSur(true);


};

void SdlHelper::updateObjetos(Mapa * mapa, int y, int x, Camara * camara)
{
	for ( int i = 0; i < mapa->estructuras.size(); i++)
	{	
		if(mapa->estructuras.at(i)->getX() == x && mapa->estructuras.at(i)->getY() == y)
		{
			switch(mapa->estructuras.at(i)->getTipo())
			{
				default:
					rectZona.x = TILE_GRANJAX*TILE_AN;
					rectZona.y = TILE_GRANJAY*TILE_AL;
					break;
				case TORRE:
					rectZona.x = TILE_TORREX*TILE_AN;
					rectZona.y = TILE_TORREY*TILE_AL;
					break;
				case ARBUSTO:
					rectZona.x = TILE_ARBUSTOX*TILE_AN;
					rectZona.y = TILE_ARBUSTOY*TILE_AL;
					break;
			};
			rectDestino.x =  (x * TILE_AN ) - camara->getX();
			rectDestino.y =  (y * TILE_AL ) - camara->getY();
			SDL_RenderCopy(render,tileset,&rectZona,&rectDestino);
		}
	}
};
