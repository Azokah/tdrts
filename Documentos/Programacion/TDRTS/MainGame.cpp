#include "MainGame.h"

MainGame::MainGame()
{
	vistaInterna = false;
	gameOn = true;
	FPS = FPSMAX = 0;
	mundo = new Mundo();
	sdl = new SdlHelper();
	timer = new Timer();
	timer->start();
	proximoTick = timer->get_ticks() + 1000;
	camara = new Camara();
	interfaz = new InterAcciones();
};

MainGame::~MainGame(){};

void MainGame::init()
{
	while(gameOn){
		sdl->input(interfaz);
			//TEMPORAL
			if(interfaz->getResetMondo())mundo->resetMundo();
			
			//Temporal no se completa todo el mapa del mundo.
			for(int i = 0; i < MUNDO_H; i++) //QUICKFIX= cambio MAPA_H = MUNDO_H
			{
				for(int j = 0; j < MUNDO_W; j++)
				{
					sdl->updateMundo(vistaInterna,mundo,mundo->mapas.front(), i, j, camara,interfaz);
				}
			}
		interfaz->run(&gameOn,&vistaInterna,camara);
		drawFPS();
		sdl->renderizar();
		sdl->limpiarRender();
		FPS++;
		mundo->randomizar();
		//SDL_Delay(250);
	}
};

void MainGame::drawFPS(){
	if(timer->get_ticks() >= proximoTick) {
		proximoTick = timer->get_ticks() + 1000;
		FPSMAX = FPS;
		FPS = 0;
	}
	char txt[25];
	sprintf(txt,"FPS: %d",FPSMAX);
	sdl->imprimirTexto(txt,25,50,255,0,125);
};


