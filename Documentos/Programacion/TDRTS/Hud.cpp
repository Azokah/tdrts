#include "Hud.h"


Hud::Hud(){
	x = 0;
	y = 0;
};

Hud::~Hud(){};

void Hud::setTexture(SDL_Texture * texture){hudTexture = texture;};
SDL_Texture * Hud::getTexture(){return hudTexture;};
int Hud::getX(){return x;};
int  Hud::getY(){return y;};
