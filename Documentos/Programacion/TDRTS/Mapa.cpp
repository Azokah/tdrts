#include "Mapa.h"


Mapa::Mapa()
{
	srand(time(NULL));
	numero = 0;

	resetMapa();
};

Mapa::~Mapa(){};

void Mapa::resetMapa()
{
	for(int i = 0;i < MAPA_H; i++)
	{
		for(int j = 0;j < MAPA_W; j++)
		{
			mapa[i][j] = MAPA_PASTO;
			if( i == 0 || i == MAPA_H -1 || j == 0 || j == MAPA_W -1) mapa[i][j] = MAPA_PARED;
			
		}

	}
};

int Mapa::getMapa(int x, int y)
{
	return mapa[y][x];
};


